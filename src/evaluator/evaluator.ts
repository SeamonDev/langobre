import { AssignementExpression, BinaryExpression, Expression, FloatExpression, IdentifierExpression, IntExpression, VarExpression } from "../parser/expressions";

const variableList: Variable[] = []

export const evaluate = (expressions: Expression[], evaluatorOptions: { print: boolean }) => {
    for (let i = 0; i < expressions.length; i++) {
        evaluateExpression(expressions[i], evaluatorOptions)
    }
}

const evaluateExpression = (expression: Expression, evaluatorOptions: { print: boolean }) => {
    switch (expression.type) {
        case "IntExpression":
            if (evaluatorOptions.print) {
                console.log(expression.value)
            }
            return expression.value
        case "FloatExpression":
            if (evaluatorOptions.print) {
                console.log(expression.value)
            }
            return expression.value
        case "BinaryExpression":
            return evaluateBinaryExpression(expression as BinaryExpression, evaluatorOptions)
        case "AssignementExpression":
            return evaluateAssignementExpression(expression as AssignementExpression, evaluatorOptions)
        case "IdentifierExpression":
            return evaluateIdentifierExpression(expression as IdentifierExpression, evaluatorOptions)
    }
}

const evaluateBinaryExpression = (expression: BinaryExpression, evaluatorOptions: { print: boolean }) => {
    switch (expression.Operator) {
        case "+": {
            const res: any = evaluateExpression(expression.left, { print: false }) + evaluateExpression(expression.right, { print: false })
            if (evaluatorOptions.print) {
                console.log(res)
            }
            return res
        }
        case "-": {
            const res: any = evaluateExpression(expression.left, { print: false }) - evaluateExpression(expression.right, { print: false })
            if (evaluatorOptions.print) {
                console.log(res)
            }
            return res
        }
        case "*": {
            const res: any = evaluateExpression(expression.left, { print: false }) * evaluateExpression(expression.right, { print: false })
            if (evaluatorOptions.print) {
                console.log(res)
            }
            return res
        }
        case "/": {
            const res: any = evaluateExpression(expression.left, { print: false }) / evaluateExpression(expression.right, { print: false })
            if (evaluatorOptions.print) {
                console.log(res)
            }
            return res
        }
    }
}

const evaluateAssignementExpression = (expression: AssignementExpression, evaluatorOptions: { print: boolean }) => {
    if (variableList.find(variable => variable.name === expression.identifier)) {
        throw "Variable already exists."
    }
    const expressionValue: any = evaluateExpression(expression.expression, { print: false })
    variableList.push({
        name: expression.identifier,
        type: null,
        value: expressionValue
    })
    if (evaluatorOptions.print) {
        console.log(expression.identifier + " -> " + expressionValue)
    }
    return expressionValue
}

const evaluateIdentifierExpression = (expression: IdentifierExpression, evaluatorOptions: { print: boolean }) => {
    const foundVariable = variableList.find(varibale => varibale.name === expression.identifier)
    if (!foundVariable) {
        throw "variable uninitialized"
    }
    if (foundVariable.value !== null) {
        if (evaluatorOptions.print) {
            console.log(foundVariable.value)
        }
        return foundVariable.value
    }
    if (evaluatorOptions.print) {
        console.log(null)
    }
    return null
}

type Variable = {
    value: Expression | null
    type: 'int' | 'float' | 'string' | null
    name: string
}