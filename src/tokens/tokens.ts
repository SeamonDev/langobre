export type Token = NumberToken |
    MinusToken | PlusToken | TimesToken | DivideToken |
    DotToken |
    AssignementToken | IdentifierToken |
    KeywordToken |
    NewlineToken | SemicolonToken | EOFToken |
    LeftParenToken | RightParenToken

export type OperatorToken = PlusToken | MinusToken | TimesToken | DivideToken

export const TokenType = {
    IntToken: 'IntToken',
    FloatToken: 'FloatToken',
    DotToken: 'DotToken',
    MinusToken: 'MinusToken',
    PlusToken: 'PlusToken',
    AssignementToken: 'AssignementToken',
    IdentifierToken: 'IdentifierToken',
    KeywordToken: 'KeywordToken',
    NewlineToken: 'NewlineToken',
    EOFToken: 'EOFToken',
    TimesToken: 'TimesToken',
    DivideToken: 'DivideToken',
    SemicolonToken: 'SemicolonToken',
    LeftParenToken: 'LeftParenToken',
    RightParenToken: 'RightParenToken'
} as const

export type TokenType = typeof TokenType

// Numbers

export type NumberToken = IntToken | FloatToken

export type IntToken = {
    value: number
    type: TokenType["IntToken"]
}

export type FloatToken = {
    value: number
    type: TokenType["FloatToken"]
}

// Variables

export type DotToken = {
    type: TokenType['DotToken']
}

// Operators

export type AssignementToken = {
    type: TokenType["AssignementToken"]
}

export type IdentifierToken = {
    value: string
    type: TokenType["IdentifierToken"]
}

export type MinusToken = {
    type: TokenType['MinusToken']
}

export type PlusToken = {
    type: TokenType['PlusToken']
}

export type TimesToken = {
    type: TokenType["TimesToken"]
}

export type DivideToken = {
    type: TokenType["DivideToken"]
}

export type KeywordToken = {
    keyword: Keyword
    type: TokenType["KeywordToken"]
}

export type LeftParenToken = {
    type: TokenType["LeftParenToken"]
}

export type RightParenToken = {
    type: TokenType["RightParenToken"]
}

export const Keyword = {
    var: 'var'
} as const

export type NewlineToken = {
    type: TokenType["NewlineToken"]
}

export type SemicolonToken = {
    type: TokenType["SemicolonToken"]
}

export type EOFToken = {
    type: TokenType["EOFToken"]
}

export type Keyword = keyof typeof Keyword