import { OperatorToken, TimesToken, DivideToken, IntToken, FloatToken, IdentifierToken, TokenType, MinusToken, PlusToken, Token } from "../tokens/tokens"
import { IntExpression, FloatExpression, IdentifierExpression, BinaryExpression, Expression, AssignementExpression } from "./expressions"
import { toThrowable } from "./parser_errors"


enum Priority {
    Default = 1,
    TimesDivide = 2,
    DefaultInParens = 3,
    TimesDivideParens = 4,
    EOF = -1
}

type Parser = {
    currentIndex: number
    nextIndex: number
    tokens: Token[]
    expressions: Expression[]
    currentFile?: string
    line: number
    offset: number,
    priority: Priority
}

const parser: Parser = {
    currentIndex: 0,
    nextIndex: 1,
    tokens: [], 
    expressions: [],
    line: 0,
    offset: 0,
    priority: Priority.Default
}

export const parse = (tokens: Token[], fileName?: string): Expression[] => {
    parser.tokens = tokens
    parser.expressions = []
    parser.currentFile = fileName
    parser.line = 1
    parser.offset = 0
    do {
        const expression = parseExpression(false)
        if (expression) {
            parser.expressions.push(expression)
        }
    } while (nextIndex())
    parser.currentIndex = 0
    parser.nextIndex = 1
    parser.line = 1
    parser.offset = 0
    return parser.expressions
}

const parseBinaryExpression = (left: Expression): [BinaryExpression, boolean] => {
    switch (parser.tokens[parser.nextIndex].type) {
        case "MinusToken": {
            parser.priority
            const minusToken = parser.tokens[parser.nextIndex] as MinusToken
            nextIndex()
            nextIndex()
            let nextToken = parser.tokens[parser.nextIndex]
            let expr: BinaryExpression
            if (isOperator(nextToken)) {
                if (isOfHigherPriority(minusToken, nextToken)) {
                    while (isOperator(nextToken) && isOfHigherPriority(minusToken, nextToken)) {
                        expr = {
                            type: "BinaryExpression",
                            left,
                            right: parseExpression(false)!,
                            Operator: '-'
                        }
                        nextToken = parser.tokens[parser.nextIndex]
                    }
                } else {
                    let right = parseIntExpression()
                    expr = {
                        left,
                        Operator: '-',
                        right,
                        type: "BinaryExpression"
                    }
                }
            } else {
                expr = {
                    type: "BinaryExpression",
                    left,
                    right: parseExpression(true)!,
                    Operator: '-'
                }
            }
            return [expr!, isOfHigherPriority(minusToken, nextToken as OperatorToken)]
        }
        case "PlusToken": {
            const plusToken = parser.tokens[parser.nextIndex] as PlusToken
            nextIndex()
            nextIndex()
            let nextToken = parser.tokens[parser.nextIndex]
            let expr: BinaryExpression
            if (isOperator(nextToken)) {
                if (isOfHigherPriority(plusToken, nextToken)) {
                    while (isOperator(nextToken) && isOfHigherPriority(plusToken, nextToken)) {
                        expr = {
                            type: "BinaryExpression",
                            left,
                            right: parseExpression(false)!,
                            Operator: '+'
                        }
                        nextToken = parser.tokens[parser.nextIndex]
                    }
                } else {
                    let right = parseIntExpression()
                    expr = {
                        left,
                        Operator: '+',
                        right,
                        type: "BinaryExpression"
                    }
                }
            } else {
                expr = {
                    type: 'BinaryExpression',
                    left,
                    right: parseExpression(false)!,
                    Operator: '+'
                }
            }
            return [expr!, isOfHigherPriority(plusToken, nextToken as OperatorToken)]
        }
        case 'TimesToken': {
            const timesToken = parser.tokens[parser.nextIndex] as TimesToken
            nextIndex()
            nextIndex()
            let nextToken = parser.tokens[parser.nextIndex]
            let expr: BinaryExpression
            if (isOperator(nextToken)) {
                if (isOfHigherPriority(timesToken, nextToken)) {
                    while (isOperator(nextToken) && isOfHigherPriority(timesToken, nextToken)) {
                        expr = {
                            type: 'BinaryExpression',
                            left,
                            right: parseExpression(false)!,
                            Operator: '*'
                        }
                        nextToken = parser.tokens[parser.nextIndex]
                    }
                } else {
                    let right = parseIntExpression()
                    expr = {
                        left,
                        Operator: '*',
                        right,
                        type: "BinaryExpression"
                    }
                }
            } else {
                expr = {
                    type: 'BinaryExpression',
                    left,
                    right: parseExpression(false)!,
                    Operator: '*'
                }
            }
            return [expr!, isOfHigherPriority(timesToken, nextToken as OperatorToken)]
        }
        case 'DivideToken': {
            const divideToken = parser.tokens[parser.nextIndex] as DivideToken
            nextIndex()
            nextIndex()
            let nextToken = parser.tokens[parser.nextIndex]
            let expr: BinaryExpression
            if (isOperator(nextToken)) {
                if (isOfHigherPriority(divideToken, nextToken)) {
                    while (isOperator(nextToken) && isOfHigherPriority(divideToken, nextToken)) {
                        expr = {
                            type: 'BinaryExpression',
                            left,
                            right: parseExpression(false)!,
                            Operator: '/'
                        }
                        nextToken = parser.tokens[parser.nextIndex]
                    }
                } else {
                    let right = parseIntExpression()
                    expr = {
                        left,
                        Operator: '/',
                        right,
                        type: "BinaryExpression"
                    }
                }
            } else {
                expr = {
                    type: 'BinaryExpression',
                    left,
                    right: parseExpression(false)!,
                    Operator: '/'
                }
            }
            return [expr!, isOfHigherPriority(divideToken, nextToken as OperatorToken)]
        }
        default:
            throw toThrowable({
                invalidToken: parser.tokens[parser.currentIndex].type,
                line: parser.line,
                offset: parser.currentIndex,
                file: parser.currentFile,
                type: "UnexpectedTokenError",
                state: "Parser"
            })
    }
}

const parseExpression = (priorityTooLow: boolean): Expression | null => {
    const currenToken = parser.tokens[parser.currentIndex]
    switch (currenToken.type) {
        case 'MinusToken':
            nextIndex()
            switch (parser.tokens[parser.currentIndex].type) {
                case 'IntToken': {
                    (parser.tokens[parser.currentIndex] as IntToken).value = -(parser.tokens[parser.currentIndex] as IntToken).value
                    let left: Expression = parseIntExpression()
                    while (isOperator(parser.tokens[parser.nextIndex])) {
                        left = parseBinaryExpression(left)[0]
                    }
                    return left
                }
                case "FloatToken": {
                    (parser.tokens[parser.currentIndex] as FloatToken).value = -(parser.tokens[parser.currentIndex] as FloatToken).value
                    let left: Expression = parseFloatExpression()
                    while (isOperator(parser.tokens[parser.nextIndex])) {
                        left = parseBinaryExpression(left)[0]
                    }
                    return left
                }
            }
            throw toThrowable({
                type: "UnexpectedTokenError",
                invalidToken: parser.tokens[parser.nextIndex].type,
                line: parser.line,
                offset: parser.offset,
                state: "Parser"
            })
        case 'PlusToken':
            nextIndex()
            switch (parser.tokens[parser.currentIndex].type) {
                case 'IntToken': {
                    let left: Expression = parseIntExpression()
                    while (isOperator(parser.tokens[parser.nextIndex])) {
                        left = parseBinaryExpression(left)[0]
                    }
                    return left
                }
                case "FloatToken": {
                    let left: Expression = parseIntExpression()
                    while (isOperator(parser.tokens[parser.nextIndex])) {
                        left = parseBinaryExpression(left)[0]
                    }
                    return left
                }
            }
            throw toThrowable({
                type: "UnexpectedTokenError",
                invalidToken: parser.tokens[parser.nextIndex].type,
                line: parser.line,
                offset: parser.offset,
                state: "Parser"
            })
        case "IntToken": // index: 0
            let left: Expression = parseIntExpression()
            while (isOperator(parser.tokens[parser.nextIndex]) && !priorityTooLow) {
                const res = parseBinaryExpression(left)
                left = res[0]
                priorityTooLow = res[1]
            }
            return left
        case "FloatToken":
            return parseFloatExpression()
        case "NewlineToken":
            parser.line++
            parser.offset = 0
            nextIndex()
            return null
        case "IdentifierToken": {            
            const expression = parseIdentifierExpression()
            if (expression.type === 'IdentifierExpression') {
                let left: BinaryExpression | IdentifierExpression = expression
                while (isOperator(parser.tokens[parser.nextIndex]) && !priorityTooLow) {
                    const res = parseBinaryExpression(left)
                    left = res[0]
                    priorityTooLow = res[1]
                }
                return left
            }
            return expression
        }
        case "KeywordToken": {
            nextIndex()
            // switch ((currenToken as KeywordToken).keyword) {
            //     case 'var': {
            //         if (parser.tokens[parser.currentIndex].type !== "IdentifierToken") {
            //             throw toThrowable({
            //                 type: "UnexpectedTokenError",
            //                 invalidToken: parser.tokens[parser.currentIndex].type,
            //                 line: parser.line,
            //                 offset: parser.offset,
            //                 state: "Parser"
            //             })
            //         }
            //         const identifier = (parser.tokens[parser.currentIndex] as IdentifierToken).value
            //         if (parser.tokens[parser.nextIndex].type === "AssignementToken") {
            //             nextIndex()
            //             nextIndex()
            //             const expression = parseExpression(false)
            //             if (!expression) {
            //                 toThrowable({
            //                     type: "UnexpectedTokenError",
            //                     invalidToken: parser.tokens[parser.currentIndex].type,
            //                     line: parser.line,
            //                     offset: parser.offset,
            //                     state: "Parser"
            //                 })
            //             }
            //             return {
            //                 type: "VarExpression",
            //                 initialized: true,
            //                 indentifierType: checkExpressionType(expression!),
            //                 identifier,
            //                 value: expression!
            //             }
            //         } else {
            //             if (parser.tokens[parser.nextIndex].type !== "NewlineToken" || parser.tokens[parser.nextIndex].type !== 'EOFToken') {
            //                 throw toThrowable({
            //                     type: "UnexpectedTokenError",
            //                     invalidToken: parser.tokens[parser.nextIndex].type,
            //                     line: parser.line,
            //                     offset: parser.offset,
            //                     state: "Parser"
            //                 })
            //             }
            //             return {
            //                 type: "VarExpression",
            //                 initialized: false,
            //                 identifier
            //             }
            //         }
            //     }
            // }
        }
        case 'EOFToken':
            return null
        case 'LeftParenToken': {
            nextIndex()
            let left = parseExpression(false)
            nextIndex()
            while (isOperator(parser.tokens[parser.nextIndex]) && !priorityTooLow) {
                const res = parseBinaryExpression(left!)
                left = res[0]
                priorityTooLow = res[1]
            }
            return left
        }
        default:
            throw toThrowable({
                type: "UnimplementedError",
                unimplementedToken: currenToken.type,
                line: parser.line,
                offset: parser.offset,
                state: "Parser",
                file: parser.currentFile
            })
    }
}

const parseIntExpression = (): IntExpression => {
    const currentToken = parser.tokens[parser.currentIndex] as IntToken
    return { type: 'IntExpression', value: currentToken.value }
}

const parseFloatExpression = (): FloatExpression => {
    const currentToken = parser.tokens[parser.currentIndex] as FloatToken
    return { type: 'FloatExpression', value: currentToken.value }
}

const parseIdentifierExpression = (): IdentifierExpression | AssignementExpression => {
    const identifier = parser.tokens[parser.currentIndex] as IdentifierToken
    if(parser.tokens[parser.nextIndex].type === 'AssignementToken') {
        nextIndex()
        nextIndex()
        if (!parser.tokens[parser.currentIndex]) {
            console.log("TODO: Expected Expression Error")
        }
        return { type: 'AssignementExpression', identifier: identifier.value, expression: parseExpression(false)! }
    }
    return { type: 'IdentifierExpression', identifier: identifier.value }
}

// const checkExpressionType = (expression: Expression): 'int' | 'float' | 'string' | null => {
//     switch (expression.type) {
//         case "IntExpression":
//             return 'int'
//         case "FloatExpression":
//             return 'float'
//         case "BinaryExpression":
//             throw 'BinaryExpression TypeCheck not yet enabled'
//         case "VarExpression":
//             throw "VarExpression TypeCheck not yet enabled"
//         case "AssignementExpression":
//             throw "AssignementExpression TypeCheck not yet enabled"
//         case "IdentifierExpression": {
//             const foundVarExpression = parser.expressions.find(expr => expr.type === 'VarExpression' && expr.identifier === expression.identifier) as VarExpression | undefined
//             const foundIdentifierType = foundVarExpression?.initialized ? foundVarExpression.indentifierType : null
//             return foundIdentifierType
//         }
//     }
// }

const nextIndex = (): boolean => { // add while condition into heree
    parser.currentIndex++
    parser.nextIndex++
    return parser.currentIndex < parser.tokens.length
}

const isOperator = (token: Token): token is OperatorToken => {
    return token.type === TokenType.PlusToken || token.type === TokenType.MinusToken || token.type === TokenType.TimesToken || token.type === TokenType.DivideToken
}

const isOfHigherPriority = (token1: OperatorToken, token2: OperatorToken): boolean => {
    return getPriority(token1) < getPriority(token2)
}

const getPriority = (operator: OperatorToken): Priority => {
    switch (operator.type) {
        case "PlusToken":
            return Priority.Default
        case "MinusToken":
            return Priority.Default
        case "TimesToken":
            return Priority.TimesDivide
        case "DivideToken":
            return Priority.TimesDivide
    }
}