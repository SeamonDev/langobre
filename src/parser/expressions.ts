export type Expression = IntExpression | FloatExpression | BinaryExpression | VarExpression | AssignementExpression | IdentifierExpression

export const ExpressionType = {
    IntExpression: 'IntExpression',
    FloatExpression: 'FloatExpression',
    BinaryExpression: 'BinaryExpression',
    VarExpression: 'VarExpression',
    AssignementExpression: 'AssignementExpression',
    IdentifierExpression: 'IdentifierExpression'
} as const

export type ExpressionType = typeof ExpressionType

export const IdentifierType = {
    string: 'string',
    float: 'float',
    int: 'int'
} as const

export type IdentifierType = keyof typeof IdentifierType;

export type Operator = '+' | '-' | '*' | '/'

// Numbers

export type NumberExpression = IntExpression | FloatExpression

export type IntExpression = {
    value: number
    type: ExpressionType["IntExpression"]
}

export type FloatExpression = {
    value: number
    type: ExpressionType["FloatExpression"]
}

export type BinaryExpression = {
    left: Expression
    right: Expression
    Operator: Operator
    type: ExpressionType["BinaryExpression"]
}

// Variables

export type VarExpression = {
    identifier: string
    initialized: true
    indentifierType: IdentifierType | null
    value: Expression
    type: ExpressionType['VarExpression']
} | {
    identifier: string
    initialized: false
    identifierType?: IdentifierType
    type: ExpressionType['VarExpression']
}

export type AssignementExpression = {
    identifier: string,
    type: ExpressionType['AssignementExpression'],
    expression: Expression
}

export type IdentifierExpression = {
    identifier: string,
    type: ExpressionType['IdentifierExpression']
}
