import type { TokenType } from "../tokens/tokens"

export type Error = UnexpectedTokenError | UnimplementedError

export const ErrorType = {
    UnexpectedTokenError: "UnexpectedTokenError",
    UnimplementedError: "UnimplementedError"
} as const

export type ErrorType = typeof ErrorType

export const ErrorState = {
    Lexer: "Lexer",
    Parser: "Parser",
    Evaluator: "Evaluator",
    Optimizer: "Optimizer",
    Minimizer: "Minimizer",
    Transpiler: "Transpiler",
    Compiler: "Compiler"
} as const

export type ErrorState = typeof ErrorState

export type UnexpectedTokenError = {
    invalidToken: keyof TokenType
    file?: string
    line: number
    offset: number
    type: ErrorType["UnexpectedTokenError"]
    state: ErrorState["Parser"]
}

const isUnexpectedTokenError = (value: any): value is UnexpectedTokenError => {
    return 'invalidToken' in value && "offset" in value && "line" in value
}

export type UnimplementedError = {
    unimplementedToken: keyof TokenType
    file?: string
    line: number
    offset: number
    type: ErrorType["UnimplementedError"]
    state: ErrorState["Parser"]
}

const isUnimplementedError = (value: any): value is UnimplementedError => {
    return 'unimplementedToken' in value && "line" in value && "offset" in value && "type" in value && "state" in value
}

export const toThrowable = (error: unknown & Error) => {
    if (isUnexpectedTokenError(error)) {
        return '[Parser Error]: Unexpected TokenType: $invalidToken.'.replace('$invalidToken', error.invalidToken) +
            '\n' +
            (
                error.file ?
                    'In $file, at line: $line, offset: $offset.'.replace("$file", error.file!).replace("$line", error.line + '').replace('$offset', error.offset + "") :
                    'At line: $line, offset: $offset.'.replace("$line", error.line + '').replace('$offset', error.offset + "")
            )
    }
    if (isUnimplementedError(error)) {
        return '[Parser Error]: Unimplemented TokenType: $unimplementedToken'.replace('$unimplementedToken', error.unimplementedToken) +
            "\n" +
            (
                error.file ?
                    "In $file, at line: $line, offset: $offset.".replace("$file", error.file!).replace("$line", error.line + "").replace("$offset", error.offset + "") :
                    "At line: $line, offset: $offset.".replace("$line", error.line + "").replace("$offset", error.offset + "")
            )
    }
}