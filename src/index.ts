import { parseLaprolaSetup } from "./config/laprola.setup"
import { evaluate } from "./evaluator/evaluator"
import { lex } from "./lexer/lexer"
import { parse } from "./parser/parser"

if (Bun.argv[2]) {
    if (Bun.argv[2] === '--help') {
        console.log(`Do bun run index.ts <filepath>`)
        process.exit()
    }
    const laprolaSetupFile = Bun.file('./laprola-setup')
    // console.log(parseLaprolaSetup(await laprolaSetupFile.text()))
    const fileRef = Bun.file(Bun.argv[2])
    if (!await fileRef.exists()) console.error('File doesn\'t exist!')
    const tokens = lex(await fileRef.text())
    const printLexer = Bun.argv.find(argument => argument === "--log-lexer")
    const printParser = Bun.argv.find(argument => argument === "--log-parser")
    if (printLexer) console.log(tokens)
    const expressions = parse(tokens, Bun.argv[2])
    if (printParser) console.log(expressions)
    if (printLexer || printParser) console.log('-----')
    console.time('ran ' + Bun.argv[2])
    evaluate(expressions, { print: true })
    console.timeLog('ran ' + Bun.argv[2])
} else {
    console.log('repl mode, write "$exit" to quit')
    for await (const line of console) {
        if (line.includes("$exit")) {
            process.exit()
        }
        const tokens = lex(line)
        const expressions = parse(tokens)
        evaluate(expressions, { print: true })
    }
}