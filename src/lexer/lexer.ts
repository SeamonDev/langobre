import { Keyword, Token, TokenType } from "../tokens/tokens"

type Lexer = {
    currentIndex: number
    nextIndex: number
    fileConent: string | null
    numberString: string
    stringBuffer: string
    tokens: Token[]
}

const lexer: Lexer = {
    currentIndex: 0,
    nextIndex: 1,
    fileConent: null,
    numberString: '',
    tokens: [],
    stringBuffer: ""
}

export const lex = (fileContent: string): Token[] => {
    lexer.fileConent = fileContent
    lexer.tokens = []
    while (lexer.currentIndex < lexer.fileConent.length) {
        const currentChar = fileContent[lexer.currentIndex]
        const nextChar = fileContent[lexer.nextIndex]
        switch (currentChar) {
            case '=':
                lexer.tokens.push({ type: TokenType.AssignementToken })
                break
            case '\n':
            case ' ':
                if (lexer.stringBuffer.length !== 0) {
                    const foundKeywordPropertyKey: Keyword = Object.keys(Keyword).find(keyword => keyword === lexer.stringBuffer) as any
                    if (foundKeywordPropertyKey) {
                        lexer.tokens.push({ type: TokenType.KeywordToken, keyword: Keyword[foundKeywordPropertyKey] })
                        lexer.stringBuffer = ""
                        break
                    }
                    lexer.tokens.push({ type: TokenType.IdentifierToken, value: lexer.stringBuffer })
                    lexer.stringBuffer = ""
                    break
                }
                if (lexer.numberString.length !== 0) {
                    lexer.tokens.push({ type: lexer.numberString.includes('.') ? TokenType.FloatToken : TokenType.IntToken, value: parseFloat(lexer.numberString) })
                    lexer.numberString = ""
                }

                break
            case '+':
                if (lexer.numberString.length !== 0) {
                    lexer.tokens.push({ type: lexer.numberString.includes('.') ? TokenType.FloatToken : TokenType.IntToken, value: parseFloat(lexer.numberString) })
                    lexer.numberString = ""
                }
                lexer.tokens.push({ type: TokenType.PlusToken })
                break
            case '/':
                if (lexer.numberString.length !== 0) {
                    lexer.tokens.push({
                        type: lexer.numberString.includes('.') ? TokenType.FloatToken : TokenType.IntToken,
                        value: parseFloat(lexer.numberString)
                    })
                }
                lexer.tokens.push({ type: TokenType.DivideToken })
                break
            case '*':
                if (lexer.numberString.length !== 0) {
                    lexer.tokens.push({
                        type: lexer.numberString.includes('.') ? TokenType.FloatToken : TokenType.IntToken,
                        value: parseFloat(lexer.numberString)
                    })
                    lexer.numberString = ''
                }
                lexer.tokens.push({ type: TokenType.TimesToken })
                break
            case '(':
                lexer.tokens.push({ type: TokenType.LeftParenToken })
                break
            case ')':
                lexer.tokens.push({ type: TokenType.RightParenToken })
                break
            case '-':
                if (lexer.numberString.length !== 0) {
                    lexer.tokens.push({ type: lexer.numberString.includes('.') ? TokenType.FloatToken : TokenType.IntToken, value: parseFloat(lexer.numberString) })
                    lexer.numberString = ""
                }
                lexer.tokens.push({ type: TokenType.MinusToken })
                break
            case '.':
                if (parseInt(nextChar) !== undefined && !Number.isNaN(parseInt(nextChar))) {
                    lexer.numberString += currentChar
                } else {
                    lexer.tokens.push({ type: TokenType.DotToken })
                }
                break
            default:
                if (parseInt(currentChar) !== undefined && !Number.isNaN(parseInt(currentChar))) {
                    if (nextChar === '.' || (parseInt(nextChar) !== undefined && !Number.isNaN(parseInt(nextChar)))) {
                        lexer.numberString += currentChar
                    } else {
                        if (lexer.numberString.length !== 0) {
                            lexer.tokens.push({
                                type: lexer.numberString.includes('.') ?
                                    TokenType.FloatToken :
                                    TokenType.IntToken,
                                value: lexer.numberString.includes('.') ?
                                    parseFloat(lexer.numberString + currentChar) :
                                    parseInt(lexer.numberString + currentChar)
                            })
                            lexer.numberString = ''
                        } else {
                            lexer.tokens.push({ type: TokenType.IntToken, value: +currentChar })
                            lexer.numberString = ''
                        }
                    }
                } else {
                    if (parseInt(nextChar) === undefined || Number.isNaN(parseInt(nextChar))) {
                        lexer.stringBuffer += currentChar
                    }
                }
        }
        lexer.currentIndex++
        lexer.nextIndex++
    }
    if (lexer.numberString.length !== 0) {
        lexer.tokens.push({ type: lexer.numberString.includes('.') ? TokenType.FloatToken : TokenType.IntToken, value: parseFloat(lexer.numberString) })
        lexer.numberString = ""
    }
    if (lexer.stringBuffer.length !== 0) {
        lexer.tokens.push({ type: TokenType.IdentifierToken, value: lexer.stringBuffer })
        lexer.stringBuffer = ""
    }
    lexer.tokens.push({ type: 'EOFToken' })
    lexer.currentIndex = 0
    lexer.nextIndex = 1
    return lexer.tokens
}


