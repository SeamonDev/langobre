export type Syntax = {
    if: IfConfiguration
    while: WhileConfiguration
    block: BlockConfiguration
    group: GroupConfiguration
    operators: OperatorConfiguration[]
    types: TypeConfiguration[]
    floatComma: string
}

export type Literal = {
    prefix: string
    suffix: string
    pattern: RegExp
}

export type TypeConfiguration = {
    name: string
    literal: Literal[]
}

export type OperatorConfiguration = {
    left: Literal
    operator: string
    right?: Literal
}

export type GroupConfiguration = {

}

export type BlockConfiguration = {

}

export type IfConfiguration = (
    {
        wrapConfition: true
    } | {
        wrapConfition: false
    }
) & {
    type: 'Expression' | 'Statement'

}

export type WhileConfiguration = {
    // keyword
}

export type InlineIfConfiguration = {
    implicitBooleanConversion: boolean
    chainable: boolean
    requireElseCase: boolean
    conditionalOperator: OperatorConfiguration
    elseOperator: OperatorConfiguration
}