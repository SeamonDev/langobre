import { Syntax } from "./laprola.setup.syntax"

export const parseLaprolaSetup = (setupFileText: string): LaprolaSetupSettings => {
    const setupLines = setupFileText.split('\n')
    const laprolaSetup: Record<keyof LaprolaSetupSettings, any> = {
        version: undefined,
        laprolaMethod: undefined,
        syntax: undefined,
        interpreterType: undefined,
        entryFile: undefined
    }
    for (const line of setupLines) {
        if (line.includes('=')) {
            const [property, value] = line.split('=')
            const valueWithoutComments = value.split('//')[0].trim()
            if (valueWithoutComments.startsWith('"') && valueWithoutComments.endsWith('"')) {
                laprolaSetup[transformPropertiy(property as any)] = valueWithoutComments.replaceAll('"', '')
            } else {
                laprolaSetup[transformPropertiy(property as any)] = valueWithoutComments
            }
        } else {

        }
    }
    if (verifyLaprolaSetupSettings(laprolaSetup)) {
        return laprolaSetup
    } else {
        throw "Laprola Setup Error"
    }
}

export type LaprolaSetupSettings = {
    version: '1.0'
    laprolaMethod: 'interpreter' | 'INTERPRETER'
    syntax: Syntax
    interpreterType: 'IMMEDIATE TEXT EVALUATION' | 'immediate text evaluation'
    entryFile: string
}

const verifyLaprolaSetupSettings = (value: unknown): value is LaprolaSetupSettings => {
    return value !== undefined && value !== null && typeof value === 'object' &&
        'version' in value && typeof value.version === 'string' &&
        'laprolaMethod' in value && typeof value.laprolaMethod === 'string' && (value.laprolaMethod === 'interpreter' || value.laprolaMethod === 'INTERPRETER') &&
        'syntax' in value &&
        'interpreterType' in value && typeof value.interpreterType === 'string' && (value.interpreterType === 'IMMEDIATE TEXT EVALUATION' || value.interpreterType === 'immediate text evaluation') &&
        'entryFile' in value && typeof value.entryFile === 'string'
}

const transformPropertiy = (uppercase: 'VERSION' | 'LAPROLA METHOD' | 'INTERPRETER TYPE' | 'ENTRY FILE' | 'SYNTAX'): keyof LaprolaSetupSettings => {
    switch (uppercase) {
        case 'VERSION':
            return 'version'
        case 'LAPROLA METHOD':
            return 'laprolaMethod'
        case 'INTERPRETER TYPE':
            return 'interpreterType'
        case 'ENTRY FILE':
            return 'entryFile'
        case 'SYNTAX':
            return 'syntax'
    }
}

// const builtInSyntax: Syntax = {

// }