/// <reference type="ESNEXT">
/// <reference type="ES2022">

(Symbol as any).dispose ??= Symbol('Symbol.dispose');

(() => {

    class Test implements Disposable {

        constructor(private onDispose: () => void) {

        }

        counter: number = 0

        doSomething() {
            console.log('done somthing' + this.counter++)
        }

        [Symbol.dispose]() {
            this.onDispose()
        }
    }

    using test = new Test(() => console.log('disposed'))
    test.doSomething()
    test.doSomething()
    test.doSomething()
})()